package com.lacky.prime.generator;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import androidx.room.Room;

import com.lacky.prime.generator.database.AppDatabase;

import java.util.Locale;


public class App extends Application {



    private AppDatabase database;
    public AppDatabase getDatabase() {
        return database;
    }
    private static App mInstance;


    private SharedPreferences preferences;
    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        preferences = PreferenceManager.getDefaultSharedPreferences(this);


        LocaleUtils.setLocale(new Locale(preferences.getString("locale", LocaleUtils.LAN_EN)));
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());

        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateConfig(this, newConfig);
    }





}
