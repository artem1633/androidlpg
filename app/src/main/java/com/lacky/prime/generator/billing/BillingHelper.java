package com.lacky.prime.generator.billing;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.android.vending.billing.IInAppBillingService;

import org.json.JSONObject;


/**
 * Created by trablone on 2/25/17.
 */

public class BillingHelper {
    public static final int REQUEST_CODE_BUY = 1234;

    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    private IInAppBillingService inAppBillingService;
    private Context context;
    private BillingListener listener;

    public BillingHelper(Context context, BillingListener listener){
        this.context = context;
        this.listener = listener;

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            inAppBillingService = IInAppBillingService.Stub.asInterface(service);
            listener.onBillingConnected(inAppBillingService);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            inAppBillingService = null;
        }
    };

    public IInAppBillingService getInAppBillingService() {
        return inAppBillingService;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_CODE_BUY) {
            if (data != null){

                int responseCode = data.getIntExtra("RESPONSE_CODE", -1);
                //Log.e("tr", "responseCode : " + responseCode);
                if (responseCode == BILLING_RESPONSE_RESULT_OK) {
                    String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
                    //String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
                    // можете проверить цифровую подпись
                    readPurchase(purchaseData);
                    listener.onBillingPurchase();
                }
            }
        }
    }

    private void readPurchase(String purchaseData) {
        try {
            JSONObject jsonObject = new JSONObject(purchaseData);
            //Utils.setLog("jsonObject: " + jsonObject);
            //String orderId = jsonObject.optString("orderId");
            //String packageName = jsonObject.getString("packageName");
            //String productId = jsonObject.getString("productId");
            //long purchaseTime = jsonObject.getLong("purchaseTime");
            //int purchaseState = jsonObject.getInt("purchaseState");
            //String developerPayload = jsonObject.optString("developerPayload");
            String purchaseToken = jsonObject.getString("purchaseToken");
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putString("pay_token", purchaseToken).apply();


            //Utils.setLog("status: " + status);

        } catch (Exception e) {

        }
    }


    public void onDestroy() {
        if (serviceConnection != null) {
            context.unbindService(serviceConnection);
        }
    }


    public String getProductId(String product){
        Log.e("tr", "product: " +product);
        String[] list = product.split("\\.");
        if (list.length == 5)
            product = list[3] + " - " + list[4];
        return product;
    }

    @SuppressLint("RestrictedApi")
    public void purchaseProduct(AppCompatActivity activity, String key) throws Exception {
        Bundle buyIntentBundle = getInAppBillingService().getBuyIntent(3, activity.getPackageName(), key, "inapp", null);
        PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

        activity.startIntentSenderForResult(pendingIntent.getIntentSender(), REQUEST_CODE_BUY, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), null);

    }

    @SuppressLint("RestrictedApi")
    public void subsProduct(AppCompatActivity activity, String key) throws Exception {

        //Log.e("tr", "key " + key);
        Bundle buyIntentBundle = getInAppBillingService().getBuyIntent(3, activity.getPackageName(), key, "subs", null);
        PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

        activity.startIntentSenderForResult(pendingIntent.getIntentSender(), REQUEST_CODE_BUY, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), null);

    }

}
