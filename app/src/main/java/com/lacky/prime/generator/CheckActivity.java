package com.lacky.prime.generator;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import com.lacky.prime.generator.database.Pass;
import com.lacky.prime.generator.database.PassDao;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_hint)
    LinearLayout layoutHint;

    @BindView(R.id.item_text)
    EditText editText;
    @BindView(R.id.layout_success)
    LinearLayout layoutSuccess;
    @BindView(R.id.layout_failure)
    LinearLayout layoutFailure;

    private boolean change = true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_check);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (change){
                    change = false;
                    Pass pass = App.getInstance().getDatabase().getPassDao().getPass(editText.getText().toString());
                    if (pass != null && !TextUtils.isEmpty(pass.getText())){
                        layoutSuccess.setVisibility(View.VISIBLE);
                        layoutFailure.setVisibility(View.GONE);
                        layoutHint.setVisibility(View.GONE);
                    }else {
                        int length = charSequence.length();
                        if (length >= 6){
                            String source = charSequence.toString();
                            boolean cap = false;
                            boolean num = false;
                            char[]  out = source.toCharArray();
                            int len = source.length();

                            for(i=0; i<len; i++){
                                if(Character.isUpperCase(out[i])){
                                    cap = true;
                                }
                            }

                            for(i=0; i<len; i++){
                                if(Character.isDigit(out[i])){
                                    num = true;
                                }
                            }

                            if (cap && num){
                                layoutSuccess.setVisibility(View.VISIBLE);
                                layoutFailure.setVisibility(View.GONE);
                                layoutHint.setVisibility(View.GONE);
                            }else {
                                layoutSuccess.setVisibility(View.GONE);
                                layoutFailure.setVisibility(View.VISIBLE);
                                layoutHint.setVisibility(View.VISIBLE);
                            }


                        }else {
                            layoutSuccess.setVisibility(View.GONE);
                            layoutFailure.setVisibility(View.VISIBLE);
                            layoutHint.setVisibility(View.VISIBLE);
                        }

                    }
                    change = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @OnClick(R.id.button_copy)
    public void copy(){
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("copy", editText.getText().toString());
        clipboardManager.setPrimaryClip(clip);
        clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                Toast.makeText(CheckActivity.this, R.string.copied, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
