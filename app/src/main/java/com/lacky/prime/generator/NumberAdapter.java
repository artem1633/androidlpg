package com.lacky.prime.generator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.ViewHolder> {

    private int[] ovals = new int[]{R.drawable.oval_1, R.drawable.oval_2, R.drawable.oval_3, R.drawable.oval_4, R.drawable.oval_5};
    public NumberAdapter(Context context) {
        this.context = context;
        list = new int[0];
    }

    private Context context;


    public int[] getList() {
        return list;
    }

    public void setList(int[] list) {
        this.list = list;
    }

    private int[] list;

    @NonNull
    @Override
    public NumberAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_number, parent, false);
        return new NumberAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NumberAdapter.ViewHolder holder, int position) {
        final int item = list[position];
        holder.textView.setText(String.valueOf(item));
        int random = new Random().nextInt(5);
        holder.textView.setBackgroundResource(ovals[random]);
    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_number)
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

