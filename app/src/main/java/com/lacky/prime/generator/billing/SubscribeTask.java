package com.lacky.prime.generator.billing;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class SubscribeTask extends AsyncTask<Void, Void, Boolean> {

    Context context;
    public static final String ITEM_TYPE_SUBS = "subs";

    private BillingHelper billingHelper;


    public SubscribeTask(Context context, BillingHelper billingHelper){
        this.context = context;
        this.billingHelper = billingHelper;
    }

    @Override
    protected Boolean doInBackground(Void... iInAppBillingServices) {

        try {
            Bundle result = billingHelper.getInAppBillingService().getSkuDetails(3, context.getPackageName(), ITEM_TYPE_SUBS, null);

            int response = getResponseCodeFromBundle(result);
            if (response == BILLING_RESPONSE_RESULT_OK) {
                return true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e("tr", "e p: " + e.getMessage());
        }


        return false;
    }

    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";

    int getResponseCodeFromBundle(Bundle b) {
        Object o = b.get(RESPONSE_CODE);
        if (o == null) {
            //logDebug("Bundle with null response code, assuming OK (known issue)");
            return BILLING_RESPONSE_RESULT_OK;
        }
        else if (o instanceof Integer) return ((Integer)o).intValue();
        else if (o instanceof Long) return (int)((Long)o).longValue();
        else {
            //logError("Unexpected type for bundle response code.");
            //logError(o.getClass().getName());
            throw new RuntimeException("Unexpected type for bundle response code: " + o.getClass().getName());
        }
    }

}
