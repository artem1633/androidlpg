package com.lacky.prime.generator;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(null);


    }

    @OnClick(R.id.item_check)
    public void check(){
        startActivity(new Intent(this, CheckActivity.class));
    }

    @OnClick(R.id.item_generate)
    public void generate(){
        startActivity(new Intent(this, GenerateActivity.class));
    }

    @OnClick(R.id.item_premium)
    public void premium(){
        startActivity(new Intent(this, PremiumActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()){
            case R.id.action_share:
                share(this);
                return true;
            case R.id.action_translate:
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, locales);

                new AlertDialog.Builder(this)
                        .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                lang = LocaleUtils.LAN_EN;
                                Locale locale = new Locale(lang);
                                switch (i){
                                    case 1:
                                        lang = LocaleUtils.LAN_RU;
                                        locale = new Locale(lang);
                                        break;
                                    case 2:
                                        lang = LocaleUtils.LAN_CN;
                                        locale = new Locale(lang);
                                        break;
                                }

                                preferences.edit().putString("locale", lang).apply();
                                Locale.setDefault(locale);
                                Configuration config = new Configuration();
                                config.locale = locale;
                                getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                                recreate();
                            }
                        })
                        .show();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    String[] locales = new String[]{"English", "Русский", "中國人"};




    public  void share(Context context){

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + context.getPackageName());
        context.startActivity(Intent.createChooser(sharingIntent, "Select app").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

    }
}
