package com.lacky.prime.generator.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
public abstract class PassDao {

    @Query("SELECT * FROM `pass` ORDER BY text DESC")
    public abstract List<Pass> getAll();

    @Query("SELECT * FROM `pass` WHERE text = :date")
    public abstract Pass getPass(String date);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(Pass... wallpapers);

    @Delete
    public abstract void delete(Pass... wallpapers);

    @Update
    public abstract void update(Pass... wallpapers);

    @Query("DELETE FROM `pass`")
    public abstract void deleteAll();
}
