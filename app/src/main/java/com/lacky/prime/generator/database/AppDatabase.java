package com.lacky.prime.generator.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {Pass.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PassDao getPassDao();

}
