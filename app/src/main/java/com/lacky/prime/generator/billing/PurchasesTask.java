package com.lacky.prime.generator.billing;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by trablone on 2/25/17.
 */

public class PurchasesTask extends AsyncTask<Void, Void, List<String>> {

    Context context;
    public static final String ITEM_TYPE_SUBS = "subs";
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";

    private BillingHelper billingHelper;
    private ArrayList<String> list;

    public static String bundle2string(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = "Bundle{";
        for (String key : bundle.keySet()) {
            string += " " + key + " => " + bundle.get(key) + ";";
        }
        string += " }Bundle";
        return string;
    }

    private String type;

    public PurchasesTask(Context context, String type, BillingHelper billingHelper) {
        this.context = context;
        this.type = type;
        this.billingHelper = billingHelper;
    }

    @Override
    protected List<String> doInBackground(Void... iInAppBillingServices) {

        String continueToken = null;

        try {

            do {
                Bundle ownedItems = billingHelper.getInAppBillingService().getPurchases(3, context.getPackageName(), type, continueToken);

                ArrayList<String> purchaseDataList = ownedItems.getStringArrayList(RESPONSE_INAPP_PURCHASE_DATA_LIST);
                if (purchaseDataList != null && purchaseDataList.size() > 0) {
                    List<String> products = new ArrayList<>();
                    for (String responseItem : purchaseDataList) {
                        JSONObject jsonObject = new JSONObject(responseItem);
                        products.add(jsonObject.getString("productId"));

                    }
                    return products;
                }

                continueToken = ownedItems.getString(INAPP_CONTINUATION_TOKEN);

            } while (!TextUtils.isEmpty(continueToken));

        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;
    }

}
