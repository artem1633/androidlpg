package com.lacky.prime.generator.billing;

/**
 * Created by trablone on 2/19/17.
 */

public class InAppProduct {

        public String productId;
        public String storeName;
        public String storeDescription;
        public String price;
        public boolean isSubscription;
        public double priceAmountMicros;
        public String currencyIsoCode;
        public boolean error;

        public String getSku() {
            return productId;
        }

        public String getType() {
            return isSubscription ? "subs" : "inapp";
        }
}
