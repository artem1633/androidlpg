package com.lacky.prime.generator.billing;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by trablone on 2/19/17.
 */

public class BillingTask extends AsyncTask<Void, Void, Billing> {
    Context context;
    String type;


    private BillingHelper billingHelper;
    private ArrayList<String> list;



    public BillingTask(Context context, BillingHelper billingHelper, String type, ArrayList<String> list){
        this.context = context;
        this.type = type;
        this.billingHelper = billingHelper;
        this.list = list;
    }

    public static String bundle2string(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = "Bundle{";
        for (String key : bundle.keySet()) {
            string += " " + key + " => " + bundle.get(key) + ";";
        }
        string += " }Bundle";
        return string;
    }

    @Override
    protected Billing doInBackground(Void... iInAppBillingServices) {

        Bundle query = new Bundle();
        query.putStringArrayList("ITEM_ID_LIST", list);
        Bundle skuDetails = null;
        Billing item = new Billing();

        try {
            skuDetails = billingHelper.getInAppBillingService().getSkuDetails(3, context.getPackageName(), type, query);
            //Log.e("tr", "data: " + bundle2string(skuDetails));

            ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
            if (responseList != null){
                item.list = new ArrayList<>();


                for (String responseItem : responseList) {
                    JSONObject jsonObject = new JSONObject(responseItem);

                    /*
                    {"productId":"by.ivanvorobei.bitmex.signals",
                    "type":"subs",
                    "price":"59,99 грн.",
                    "price_amount_micros":59990000,
                    "price_currency_code":"UAH",
                    "subscriptionPeriod":"P1W",
                    "freeTrialPeriod":"P1W",
                    "title":"Полный доступ (Bitmex Cигналы)",
                    "description":"Получай все сигналы и трейдерскую информацию для Bitmex"}
                     */

                    InAppProduct product = new InAppProduct();
                    // "com.example.myapp_testing_inapp1"
                    product.productId = jsonObject.getString("productId");
                    // Покупка
                    product.storeName = jsonObject.getString("title");
                    // Детали покупки
                    product.storeDescription = jsonObject.getString("description");
                    // "0.99USD"
                    product.price = jsonObject.getString("price");
                    // "true/false"
                    product.isSubscription = jsonObject.getString("type").equals("subs");
                    // "990000" = цена x 1000000
                    product.priceAmountMicros = Double.parseDouble(jsonObject.getString("price_amount_micros")) / 1000000;
                    // USD
                    product.currencyIsoCode = jsonObject.getString("price_currency_code");


                    item.list.add(product);
                }
            }

        } catch (RemoteException | JSONException e) {
            Log.e("tr", "eb: " + e.getMessage());
        }catch (Throwable e){
            Log.e("tr", "eb: " + e.getMessage());
        }


        return item;
    }

}
