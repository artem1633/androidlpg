package com.lacky.prime.generator;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GenerateSettingFragment extends Fragment {
    @BindView(R.id.switch_char)
    Switch switchChar;
    @BindView(R.id.switch_number)
    Switch switchNumber;
    @BindView(R.id.switch_symbols)
    Switch switchSymbols;

    @BindView(R.id.item_sec_result)
    TextView textSecResult;
    @BindView(R.id.item_count)
    SeekBar seekBar;

    private SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_setting_generate, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        switchSymbols.setChecked(preferences.getBoolean("switchSymbols", false));
        switchChar.setChecked(preferences.getBoolean("switchChar", true));
        switchNumber.setChecked(preferences.getBoolean("switchNumber", true));
        seekBar.setProgress(preferences.getInt("seekBar", 10));
        textSecResult.setText(String.valueOf(preferences.getInt("seekBar", 10)));
        switchNumber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean("switchNumber", b).apply();
            }
        });
        switchChar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean("switchChar", b).apply();
            }
        });
        switchSymbols.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.edit().putBoolean("switchSymbols", b).apply();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textSecResult.setText(String.valueOf(i));
                preferences.edit().putInt("seekBar", i).apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @OnClick(R.id.button_next)
    public void next(){
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_generate, new FragmentGenerateResult())
                .commitAllowingStateLoss();
    }
}
