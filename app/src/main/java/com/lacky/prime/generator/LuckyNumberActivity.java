package com.lacky.prime.generator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LuckyNumberActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.item_sec_result)
    TextView textSecResult;
    @BindView(R.id.item_count)
    SeekBar seekBar;

    @BindView(R.id.item_sec_min)
    TextView textSecMin;
    @BindView(R.id.item_sec_max)
    TextView textSecMax;
    @BindView(R.id.rangeSeekbar1)
    CrystalRangeSeekbar rangeSeekBar;

    private SharedPreferences preferences;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lucky);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_lucky);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        seekBar.setProgress(preferences.getInt("seekBarCount", 0));
        textSecResult.setText(String.valueOf(preferences.getInt("seekBarCount", 0)));

        rangeSeekBar.setMaxStartValue(preferences.getInt("seekBarCountRangeMax", 100));
        rangeSeekBar.setMinStartValue(preferences.getInt("seekBarCountRangeMin", 0));
        rangeSeekBar.setMinValue(0);
        rangeSeekBar.setMaxValue(100);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textSecResult.setText(String.valueOf(i));
                preferences.edit().putInt("seekBarCount", i).apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rangeSeekBar.setLeftThumbColor(getResources().getColor(R.color.colorAccent));
        rangeSeekBar.setRightThumbColor(getResources().getColor(R.color.colorAccent));
        rangeSeekBar.setBarColor(getResources().getColor(R.color.colorAccentLight));
        rangeSeekBar.setBarHighlightColor(getResources().getColor(R.color.colorAccent));
        rangeSeekBar.setCornerRadius(6);

        rangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                textSecMin.setText(String.valueOf(minValue.intValue()));
                textSecMax.setText(String.valueOf(maxValue.intValue()));
                preferences.edit().putInt("seekBarCountRangeMin", minValue.intValue()).apply();
                preferences.edit().putInt("seekBarCountRangeMax", maxValue.intValue()).apply();
            }
        });



    }

    @OnClick(R.id.button_next)
    public void next(){
        startActivity(new Intent(this, LuckyNumberResult.class));

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
