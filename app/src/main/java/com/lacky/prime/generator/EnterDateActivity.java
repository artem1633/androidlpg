package com.lacky.prime.generator;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterDateActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.item_day)
    Spinner editDate;
    @BindView(R.id.item_month)
    Spinner editMonth;
    @BindView(R.id.item_year)
    Spinner editYear;

    String[] days;
    String[] months;
    String[] years;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_date);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationIcon(R.mipmap.close_date);

        Calendar calendar = Calendar.getInstance();

        days = new String[31];
        months = new String[12];
        years = new String[100];

        int day = preferences.getInt("selectDay", 0);
        int month = preferences.getInt("selectMonth", 0);
        int year = preferences.getInt("selectYear", 0);

        for (int i = 1; i <= 31; i++){
            days[i-1] = String.valueOf(i);

        }
        for (int i = 1; i <= 12; i++){
            months[i-1] = String.valueOf(i);

        }

        int ye = calendar.get(Calendar.YEAR);

        for (int i = 0; i < 100; i++){
            int y = ye - i;
            years[i] = String.valueOf(y);

        }

        ArrayAdapter<String> arrayAdapterDay = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, days);
        ArrayAdapter<String> arrayAdapterMonth = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, months);
        ArrayAdapter<String> arrayAdapterYear = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, years);

        editYear.setAdapter(arrayAdapterYear);
        editMonth.setAdapter(arrayAdapterMonth);
        editDate.setAdapter(arrayAdapterDay);

        editYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                preferences.edit().putInt("selectYear", i).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        editMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                preferences.edit().putInt("selectMonth", i).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                preferences.edit().putInt("selectDay", i).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        editDate.setSelection(day);
        editMonth.setSelection(month);
        editYear.setSelection(year);


    }

    @OnClick(R.id.button_next)
    public void next(){
        String day = days[editDate.getSelectedItemPosition()];
        String month = months[editMonth.getSelectedItemPosition()];
        String year = years[editYear.getSelectedItemPosition()];

        startActivity(new Intent(this, SelectTypeActivity.class));


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
