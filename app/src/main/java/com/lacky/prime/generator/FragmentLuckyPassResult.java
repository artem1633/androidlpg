package com.lacky.prime.generator;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.lacky.prime.generator.database.Pass;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.CLIPBOARD_SERVICE;

public class FragmentLuckyPassResult extends Fragment {


    private String[] listNumber = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private String[] listChars = new String[]{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m"};
    private String[] listCharsB = new String[]{"Q", "W", "E", "R", "T", "y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M"};
    private String[] listsymbols = new String[]{"[", "]", "{", "}", ";", ":", "'", "\"", ",", ".", "/", "<", ">", "?", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "+", "="};
    private List<String[]> list;

    private SharedPreferences preferences;
    private int count;

    @BindView(R.id.item_text)
    TextView textView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_lucky_pass_result, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        list = new ArrayList<>();

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        count = new Random().nextInt(15);
        list.add(listsymbols);
        list.add(listChars);
        list.add(listCharsB);
        list.add(listNumber);


        StringBuilder builder = new StringBuilder();
        if (list.size() > 0){
            for (int i = 0; i < count; i++){
                int min = 0;
                int max = list.size() - 1;
                int random = new Random().nextInt((max - min) + 1) + min;
                String[] listCurrent = list.get(random);
                max = listCurrent.length -1;
                random = new Random().nextInt((max - min) + 1) + min;
                builder.append(listCurrent[random]);
            }

        }

        textView.setText(builder.toString());

        if (!TextUtils.isEmpty(textView.getText().toString())){
            Pass pass = new Pass();
            pass.setText(textView.getText().toString());
            App.getInstance().getDatabase().getPassDao().insert(pass);
        }

    }

    @OnClick(R.id.button_next)
    public void next(){
        Intent intent = new Intent(getContext(), EnterDateActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R.id.button_copy)
    public void copy(){
        ClipboardManager clipboardManager = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("copy", textView.getText().toString());
        clipboardManager.setPrimaryClip(clip);
        clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                Toast.makeText(getContext(), R.string.copied, Toast.LENGTH_SHORT).show();
            }
        });
    }

}