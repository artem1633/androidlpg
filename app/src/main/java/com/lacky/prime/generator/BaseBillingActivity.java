package com.lacky.prime.generator;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.vending.billing.IInAppBillingService;
import com.lacky.prime.generator.billing.BillingHelper;
import com.lacky.prime.generator.billing.BillingListener;
import com.lacky.prime.generator.billing.PurchasesTask;


import java.util.ArrayList;
import java.util.List;

public abstract class BaseBillingActivity extends BaseActivity {

    public abstract void onPurchase();
    public abstract void onInit();

    public BillingHelper getBillingHelper() {
        return billingHelper;
    }

    public void setBillingHelper(BillingHelper billingHelper) {
        this.billingHelper = billingHelper;
    }

    public BillingHelper billingHelper;

    public ArrayList<String> getIds() {
        return ids;
    }

    public ArrayList<String> ids;

    public boolean isBilling() {
        return billing;
    }

    private boolean billing;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ids = new ArrayList<>();

        ids.add("196240aa11");

        billingHelper = new BillingHelper(this, new BillingListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onBillingConnected(IInAppBillingService inAppBillingService) {
                billing = true;

                getBillPrice();

                //new BillingTask(BaseBillingActivity.this, billingHelper, "subs", ids).execute();
            }

            @Override
            public void onBillingPurchase() {

                onPurchase();
            }
        });

    }

    public void subscribe(String id) {
        try {
            billingHelper.subsProduct(this, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void purchase(String id) {
        try {
            billingHelper.purchaseProduct(this, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getBillPrice() {
        new PurchasesTask(this, PurchasesTask.ITEM_TYPE_SUBS, billingHelper) {
            @Override
            protected void onPostExecute(List<String> strings) {
                super.onPostExecute(strings);
                if (strings != null && strings.size() > 0){
                    onInit();
                }

            }
        }.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (billingHelper != null) {
            billingHelper.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (billingHelper != null) {
            billingHelper.onDestroy();
        }
        super.onDestroy();
    }
}
