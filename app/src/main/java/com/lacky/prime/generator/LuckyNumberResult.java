package com.lacky.prime.generator;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LuckyNumberResult extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private ResultAdapter adapter;

    private SharedPreferences preferences;
    private StringBuilder builder;
    private List<String> ignoreList;
    private List<int[]> list;
    boolean five = true;
    int currentCount = 0;
    int[] item = null;
    int count;
    int max;
    int min;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lucky_result);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        ButterKnife.bind(this);

        ignoreList = new ArrayList<>();
        ignoreList.add("4");
        ignoreList.add("6");
        ignoreList.add("14");
        ignoreList.add("66");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        count = preferences.getInt("seekBarCount", 0);
        max = preferences.getInt("seekBarCountRangeMax", 100);
        min = preferences.getInt("seekBarCountRangeMin", 0);

        adapter = new ResultAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);


        builder = new StringBuilder();
        list = new ArrayList<>();

        generate();


        adapter.setList(list);

    }

    private int getRandom() {
        int random = new Random().nextInt((max - min) + 1) + min;

        String[] randoms = builder.toString().split(" ");
        if (randoms.length > 0) {

            for (String r : ignoreList){
                if (r.equals(String.valueOf(random))) {
                    Log.e("tr", "re random ignore " + random);
                    return getRandom();
                }
            }
            for (String r : randoms) {

                if (r.equals(String.valueOf(random))) {
                    Log.e("tr", "re random " + random);
                    return getRandom();
                }
            }


        }

        return random;
    }

    private void generate() {
        for (int i = 0; i < count; i++) {

            int random = getRandom();

            builder.append(random);
            if (i < count - 1) {
                builder.append(" ");
            }

            if (five) {
                if (item == null) {
                    item = new int[5];
                    currentCount = 0;
                }

                item[currentCount] = random;

                if (currentCount == 4 | i == count - 1) {
                    five = false;

                    int f = 0;
                    for (int t = 0; t < item.length; t++) {
                        if (item[t] != 0) {
                            f += 1;
                        }

                    }

                    int[] cur = new int[f];

                    for (int t = 0; t < f; t++) {
                        cur[t] = item[t];
                    }
                    list.add(cur);
                    item = null;
                }

                currentCount += 1;

            } else {
                if (item == null) {
                    item = new int[4];

                    currentCount = 0;
                }

                item[currentCount] = random;


                if (currentCount == 3 | i == count - 1) {
                    five = true;
                    int f = 0;
                    for (int t = 0; t < item.length; t++) {
                        if (item[t] != 0) {
                            f += 1;
                        }
                    }

                    int[] cur = new int[f];

                    for (int t = 0; t < f; t++) {
                        cur[t] = item[t];
                    }
                    list.add(cur);
                    item = null;
                }

                currentCount += 1;
            }
        }
    }


    @OnClick(R.id.button_next)
    public void next() {
        Intent intent = new Intent(this, LuckyNumberActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @OnClick(R.id.button_coppy)
    public void copy() {
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("copy", builder.toString());
        clipboardManager.setPrimaryClip(clip);
        clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                Toast.makeText(LuckyNumberResult.this, R.string.copied, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
